package com.example.demo2;

import com.example.demo2.models.Person;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Arrays;
import java.util.Optional;

@Controller
public class NameExchangeController {

    private Person[] persons = new Person[]{
            new Person(1L, "Petr", "Kmoch", "programator"),
            new Person(2L, "Jan", "Novak", "scrum_master")
    };

    @GetMapping(value = "/person/firstname")
    public ResponseEntity<String> readFirstNameById(@RequestParam Long name) {
        final Optional<Person> first = Arrays.stream(persons).filter(one -> one.getId().equals(name)).findFirst();
        if (notFoundEvaluate(first)) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<String>(first.get().getName(), HttpStatus.OK);
    }

    @GetMapping(value = "/person/surname")
    public ResponseEntity<String> readSurNameById(@RequestParam Long name) {
        final Optional<Person> first = Arrays.stream(persons).filter(one -> one.getId().equals(name)).findFirst();
        if (notFoundEvaluate(first)) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<String>(first.get().getSurname(), HttpStatus.OK);
    }

    @GetMapping(value = "/person/me")
    public ResponseEntity<Person> readPersonById(@RequestParam Long name) {
        final Optional<Person> first = Arrays.stream(persons).filter(one -> one.getId().equals(name)).findFirst();
        if (notFoundEvaluate(first)) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<Person>(first.get(), HttpStatus.OK);
    }

    private boolean notFoundEvaluate(Optional<Person> first) {
        if (!first.isPresent()) {
            return true;
        }
        return false;
    }


    @GetMapping(value = "/person/profese")
    public ResponseEntity<String> readProfessionByName(@RequestParam Long name) {
        final Optional<Person> first = Arrays.stream(persons).filter(one -> one.getId().equals(name)).findFirst();
        if (notFoundEvaluate(first)) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<String>(first.get().getProfession(), HttpStatus.OK);
    }


}

package com.example.demo2;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;

public class ExampleAggregationStrategy implements AggregationStrategy {
    @Override
    public Exchange aggregate(Exchange original, Exchange resource) {
        String originalBody = String.valueOf(original.getIn().getBody());


        String resourceResponse = null;
        try {
            resourceResponse = IOUtils.toString((InputStream) resource.getIn().getBody());
        } catch (IOException e) {
            e.printStackTrace();
        }

        Object mergeResult = originalBody + " " + resourceResponse;

        original.getOut().setHeaders(original.getIn().getHeaders());
        resource.getIn().setHeaders(original.getIn().getHeaders());
        resource.getOut().setHeaders(original.getIn().getHeaders());

        if (original.getPattern().isOutCapable()) {
            original.getOut().setBody(mergeResult);
        } else {
            original.getIn().setBody(mergeResult);
        }

        return original;
    }
}

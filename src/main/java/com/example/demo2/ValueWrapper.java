package com.example.demo2;

import lombok.Data;


public class ValueWrapper {
    private String value;

    public Long getValue() {
        return Long.valueOf(value);
    }

    public void setValue(String value) {
        this.value = value;
    }
}

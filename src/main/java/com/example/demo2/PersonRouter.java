package com.example.demo2;

import com.example.demo2.models.Person;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.model.rest.RestBindingMode;
import org.apache.camel.model.rest.RestOperationParamDefinition;
import org.apache.camel.model.rest.RestParamType;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import javax.jms.ConnectionFactory;

@Component
public class PersonRouter extends RouteBuilder {
    @Override
    public void configure() throws Exception {

        RestOperationParamDefinition param = new RestOperationParamDefinition();
        param.setType(RestParamType.query);
        param.setName("name");

        restConfiguration().component("servlet").bindingMode(RestBindingMode.json);
        rest().get("hello2?name={name}").param(param).to("direct:hello2");
        final ValueWrapper valueWrapper = new ValueWrapper();

        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("admin", "admin",
                "tcp://localhost:61616");
        getContext().addComponent("jms",
                JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));

        JacksonDataFormat jsonDataFormat = new JacksonDataFormat(Person.class);

        from("direct:hello2")
                .log("Http Route started")
                .setHeader(Exchange.HTTP_METHOD).constant(HttpMethod.GET)

                .to("http://localhost:8086/person/firstname?name={header.name}&bridgeEndpoint=true")
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        valueWrapper.setValue((String) exchange.getIn().getHeaders().get("name"));
                    }
                })
                .convertBodyTo(String.class)

                //.unmarshal().json(JsonLibrary.Jackson)
                .enrich("http://localhost:8086/person/surname?name={header.name}&bridgeEndpoint=true", new ExampleAggregationStrategy())
                //.unmarshal().json(JsonLibrary.Jackson)

                .convertBodyTo(String.class)
                .setHeader(Exchange.HTTP_METHOD).constant(HttpMethod.GET)
                .setHeader("name", () -> valueWrapper.getValue())

                .pollEnrich("http://localhost:8086/person/me?name=2&bridgeEndpoint=true", new ExampleAggregationStrategy())
                .marshal(jsonDataFormat)
                .convertBodyTo(String.class)
                .log(LoggingLevel.INFO, "Response : ${body}")
                //.marshal().json(JsonLibrary.Jackson)
                .to(ExchangePattern.InOnly,"jms:queue:activemq/queue/testQueue");
    }
}

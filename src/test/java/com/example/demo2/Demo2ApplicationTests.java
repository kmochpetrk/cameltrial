package com.example.demo2;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class Demo2ApplicationTests {

    @Test
    public void contextLoads() {
    }

    @Test
    public void test2() {
        LoggerFactory.getLogger(Demo2ApplicationTests.class).info("TEst probehl");
    }

    @Test
    public void testCf() throws ExecutionException, InterruptedException {
        final RestTemplate restTemplate = new RestTemplateBuilder().build();

        final Long id = 1L;

        CompletableFuture<String> completableFuture1
                = CompletableFuture.supplyAsync(() -> restTemplate.getForEntity("http://localhost:8086/person/firstname?name=" + id, String.class).getBody());

        final CompletableFuture<String> completableFuture2 = CompletableFuture.supplyAsync(() -> restTemplate.getForEntity("http://localhost:8086/person/surname?name=" + id, String.class).getBody());

        final CompletableFuture<String> completableFuture3 = CompletableFuture.supplyAsync(() -> restTemplate.getForEntity("http://localhost:8086/person/profese?name=" + id, String.class).getBody());


        CompletableFuture.allOf(completableFuture1, completableFuture3, completableFuture2).join();

        assertEquals("Petr Kmoch programator", completableFuture1.get() + " " + completableFuture2.get() + " " +completableFuture3.get());

    }

}
